﻿namespace Testing_App
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.nODESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nodesDataSet = new Testing_App.nodesDataSet();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.iDEDGEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nODEOUTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nODEINDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wEIGHTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eDGESBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nODESTableAdapter = new Testing_App.nodesDataSetTableAdapters.NODESTableAdapter();
            this.eDGESTableAdapter = new Testing_App.nodesDataSetTableAdapters.EDGESTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ComboStart = new System.Windows.Forms.ComboBox();
            this.nODESBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ComboFinish = new System.Windows.Forms.ComboBox();
            this.nODESBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nodesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eDGESBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.xDataGridViewTextBoxColumn,
            this.yDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.nODESBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 40);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(110, 225);
            this.dataGridView1.TabIndex = 0;
            // 
            // nODESBindingSource
            // 
            this.nODESBindingSource.DataMember = "NODES";
            this.nODESBindingSource.DataSource = this.nodesDataSet;
            // 
            // nodesDataSet
            // 
            this.nodesDataSet.DataSetName = "nodesDataSet";
            this.nodesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDEDGEDataGridViewTextBoxColumn,
            this.nODEOUTDataGridViewTextBoxColumn,
            this.nODEINDataGridViewTextBoxColumn,
            this.wEIGHTDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.eDGESBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(197, 40);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersVisible = false;
            this.dataGridView2.Size = new System.Drawing.Size(247, 225);
            this.dataGridView2.TabIndex = 1;
            // 
            // iDEDGEDataGridViewTextBoxColumn
            // 
            this.iDEDGEDataGridViewTextBoxColumn.DataPropertyName = "ID_EDGE";
            this.iDEDGEDataGridViewTextBoxColumn.HeaderText = "#";
            this.iDEDGEDataGridViewTextBoxColumn.Name = "iDEDGEDataGridViewTextBoxColumn";
            this.iDEDGEDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDEDGEDataGridViewTextBoxColumn.Visible = false;
            this.iDEDGEDataGridViewTextBoxColumn.Width = 30;
            // 
            // nODEOUTDataGridViewTextBoxColumn
            // 
            this.nODEOUTDataGridViewTextBoxColumn.DataPropertyName = "NODE_OUT";
            this.nODEOUTDataGridViewTextBoxColumn.HeaderText = "Начало Вершины";
            this.nODEOUTDataGridViewTextBoxColumn.Name = "nODEOUTDataGridViewTextBoxColumn";
            this.nODEOUTDataGridViewTextBoxColumn.ReadOnly = true;
            this.nODEOUTDataGridViewTextBoxColumn.Width = 75;
            // 
            // nODEINDataGridViewTextBoxColumn
            // 
            this.nODEINDataGridViewTextBoxColumn.DataPropertyName = "NODE_IN";
            this.nODEINDataGridViewTextBoxColumn.HeaderText = "Конец вершины";
            this.nODEINDataGridViewTextBoxColumn.Name = "nODEINDataGridViewTextBoxColumn";
            this.nODEINDataGridViewTextBoxColumn.ReadOnly = true;
            this.nODEINDataGridViewTextBoxColumn.Width = 75;
            // 
            // wEIGHTDataGridViewTextBoxColumn
            // 
            this.wEIGHTDataGridViewTextBoxColumn.DataPropertyName = "WEIGHT";
            this.wEIGHTDataGridViewTextBoxColumn.HeaderText = "Вес Вершины";
            this.wEIGHTDataGridViewTextBoxColumn.Name = "wEIGHTDataGridViewTextBoxColumn";
            this.wEIGHTDataGridViewTextBoxColumn.ReadOnly = true;
            this.wEIGHTDataGridViewTextBoxColumn.Width = 75;
            // 
            // eDGESBindingSource
            // 
            this.eDGESBindingSource.DataMember = "EDGES";
            this.eDGESBindingSource.DataSource = this.nodesDataSet;
            // 
            // nODESTableAdapter
            // 
            this.nODESTableAdapter.ClearBeforeFill = true;
            // 
            // eDGESTableAdapter
            // 
            this.eDGESTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(28, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Вершины";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(293, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Рёбра";
            // 
            // ComboStart
            // 
            this.ComboStart.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.nODESBindingSource1, "ID", true));
            this.ComboStart.DataSource = this.nODESBindingSource1;
            this.ComboStart.DisplayMember = "ID";
            this.ComboStart.Location = new System.Drawing.Point(12, 322);
            this.ComboStart.Name = "ComboStart";
            this.ComboStart.Size = new System.Drawing.Size(124, 21);
            this.ComboStart.TabIndex = 4;
            this.ComboStart.ValueMember = "ID";
            // 
            // nODESBindingSource1
            // 
            this.nODESBindingSource1.DataMember = "NODES";
            this.nODESBindingSource1.DataSource = this.nodesDataSet;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 306);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Начальная Вершина";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 362);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Конечная вершина";
            // 
            // ComboFinish
            // 
            this.ComboFinish.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.nODESBindingSource2, "ID", true));
            this.ComboFinish.DataSource = this.nODESBindingSource2;
            this.ComboFinish.DisplayMember = "ID";
            this.ComboFinish.FormattingEnabled = true;
            this.ComboFinish.Location = new System.Drawing.Point(12, 379);
            this.ComboFinish.Name = "ComboFinish";
            this.ComboFinish.Size = new System.Drawing.Size(124, 21);
            this.ComboFinish.TabIndex = 7;
            this.ComboFinish.ValueMember = "ID";
            // 
            // nODESBindingSource2
            // 
            this.nODESBindingSource2.DataMember = "NODES";
            this.nODESBindingSource2.DataSource = this.nodesDataSet;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(197, 344);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(247, 78);
            this.button1.TabIndex = 8;
            this.button1.Text = "Нахождение минимального маршрута между Вершинами";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxResult.Location = new System.Drawing.Point(12, 450);
            this.textBoxResult.Multiline = true;
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxResult.Size = new System.Drawing.Size(432, 49);
            this.textBoxResult.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(142, 425);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(190, 18);
            this.label5.TabIndex = 11;
            this.label5.Text = "Результирующий маршрут";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 405);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(124, 38);
            this.button2.TabIndex = 12;
            this.button2.Text = "Переключение на ручной ввод";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(12, 323);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(124, 20);
            this.textBox1.TabIndex = 13;
            this.textBox1.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(12, 380);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(124, 20);
            this.textBox2.TabIndex = 14;
            this.textBox2.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(197, 271);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(247, 23);
            this.button3.TabIndex = 15;
            this.button3.Text = "Удаленние невалидных рёбер";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // xDataGridViewTextBoxColumn
            // 
            this.xDataGridViewTextBoxColumn.DataPropertyName = "X";
            this.xDataGridViewTextBoxColumn.HeaderText = "X";
            this.xDataGridViewTextBoxColumn.Name = "xDataGridViewTextBoxColumn";
            this.xDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // yDataGridViewTextBoxColumn
            // 
            this.yDataGridViewTextBoxColumn.DataPropertyName = "Y";
            this.yDataGridViewTextBoxColumn.HeaderText = "Y";
            this.yDataGridViewTextBoxColumn.Name = "yDataGridViewTextBoxColumn";
            this.yDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(456, 511);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ComboFinish);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ComboStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.MaximumSize = new System.Drawing.Size(472, 550);
            this.MinimumSize = new System.Drawing.Size(16, 472);
            this.Name = "Form1";
            this.Text = "Минимальный маршрут между вершинами";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nodesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eDGESBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nODESBindingSource2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private nodesDataSet nodesDataSet;
        private System.Windows.Forms.BindingSource nODESBindingSource;
        private nodesDataSetTableAdapters.NODESTableAdapter nODESTableAdapter;
        private System.Windows.Forms.BindingSource eDGESBindingSource;
        private nodesDataSetTableAdapters.EDGESTableAdapter eDGESTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ComboStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ComboFinish;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource nODESBindingSource1;
        private System.Windows.Forms.BindingSource nODESBindingSource2;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDEDGEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nODEOUTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nODEINDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wEIGHTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn xDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yDataGridViewTextBoxColumn;
    }
}

