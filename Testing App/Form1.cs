﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
    

namespace Testing_App
{
    public partial class Form1 : Form
    {
        
        SqlConnection sqlConnestion;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "nodesDataSet.EDGES". При необходимости она может быть перемещена или удалена.
            this.eDGESTableAdapter.Fill(this.nodesDataSet.EDGES);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "nodesDataSet.NODES". При необходимости она может быть перемещена или удалена.
            this.nODESTableAdapter.Fill(this.nodesDataSet.NODES);

        }        

        public struct Nodes //Структура характеристик вершин и рёбер исходящих из неё
        {
            public long x; 
            public long y;
            public List<Edges> edges;  
            
            public Nodes (long n, long m)
            {
                x = n;
                y = m;
                edges = new List<Edges>();
            }   

        }

        public struct Edges //Структура характеристик вершин хранимых внутри структуры Вершин
        {
            public long destination;
            public double weight;

            public Edges(long x, double y)
            {
                this.destination = x;
                this.weight = y;
            }

        }

        //Эвристическая функция А*, возващает растояние напрямую по плоскости через теорему Пифагора
        private double bird_fly(long x_in, long y_in, long x_out, long y_out) 
        {
            return Math.Sqrt(Math.Pow(x_in - x_out, 2) + Math.Pow(y_in - y_out, 2));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Инициализация стартовой и финальной вершины из выпадающего списка или полей ввода
            long node_start;
            long node_finish;
            if ((ComboStart.Enabled == true) & (ComboFinish.Enabled == true))
            {
                node_start = Int64.Parse(ComboStart.SelectedValue.ToString()); 
                node_finish = Int64.Parse(ComboFinish.SelectedValue.ToString()); 
            }
            else
            {
                try
                {
                    node_start = Int64.Parse(textBox1.Text);
                    node_finish = Int64.Parse(textBox2.Text);
                }
                catch
                {
                    MessageBox.Show("Провал ручного ввода");
                    return;
                }
            }

            //Инициализация списка вершин, заполнение его из БД
            SortedList<long, Nodes> star_list = new SortedList<long, Nodes>();
            string connString = Properties.Settings.Default.nodesConnectionString;
            using (sqlConnestion = new SqlConnection(connString))
            {
                sqlConnestion.Open();

                
                using (SqlCommand command = new SqlCommand("SELECT * FROM NODES", sqlConnestion))
                {
                    using (SqlDataReader sqlReader = command.ExecuteReader())
                    {
                        while (sqlReader.Read())
                        {
                            star_list.Add(Convert.ToInt64(sqlReader["ID"]), new Nodes(Convert.ToInt64(sqlReader["X"]), Convert.ToInt64(sqlReader["Y"])));
                        }
                    }
                }

                //Заполнение списка рёбер каждой вершины
                using (SqlCommand command = new SqlCommand("SELECT NODE_OUT, NODE_IN, WEIGHT FROM EDGES", sqlConnestion))
                {
                    using (SqlDataReader sqlReader = command.ExecuteReader())
                    {
                        while (sqlReader.Read())
                        {
                            star_list[Convert.ToInt64(sqlReader["NODE_OUT"])].edges.Add(new Edges(Convert.ToInt64(sqlReader["NODE_IN"]), Convert.ToDouble(sqlReader["WEIGHT"])));
                        }
                    }
                }


                //Инициализация открытого, закрытого и маршрутного списков, а так же стартовых значений для А*
                SortedList<long, double> open = new SortedList<long, double>();
                List<long> close = new List<long>();
                SortedList<long, long> road_map = new SortedList<long, long>();

                open.Add(node_start, 0);
                road_map.Add(node_start, -1);
                long curr = 0;
                bool continue_flag = true;

                //Проход алгоритма А*
                while (continue_flag)
                {
                    //Поиск минимального веса маршрута + эвристики
                    double min = Double.PositiveInfinity;
                    foreach (var opn in open)
                    {
                        if (opn.Value < min)
                        {
                            min = opn.Value;
                            curr = opn.Key;
                        }
                    }

                    //Проверка всех соседей пограничной вершины на предмет веса их маршрута + эвристики                                      
                    foreach (Edges edge in star_list[curr].edges)
                    {
                        if (close.Contains(edge.destination) == false) //Если вершина не находится в закрытом списке, то её характеристика сравнивается от текущей вершины 
                        {  
                            if (open.ContainsKey(edge.destination))
                            {
                                if (open[edge.destination] > open[curr] + edge.weight + bird_fly(star_list[edge.destination].x, star_list[edge.destination].y, star_list[curr].x, star_list[curr].y))
                                {
                                    open[edge.destination] = open[curr] + edge.weight + bird_fly(star_list[edge.destination].x, star_list[edge.destination].y, star_list[curr].x, star_list[curr].y);
                                    road_map[edge.destination] = curr;
                                }
                            }
                            else
                            {
                                open.Add(edge.destination, open[curr] + edge.weight + bird_fly(star_list[edge.destination].x, star_list[edge.destination].y, star_list[curr].x, star_list[curr].y));
                                road_map.Add(edge.destination, curr);
                            }
                        }
                    }

                    //После итерации цикла пограничная вершина заносится из открытого в закрытый список
                    open.Remove(curr);
                    close.Add(curr);

                    //Провека окончания алгоритма
                    if ((open.Count == 0) | (curr == node_finish)) { continue_flag = false; }
                }


                //Формирование результирующего маршрута согласно маршрутного списка
                continue_flag = true;
                textBoxResult.Text = "";
                if (road_map.ContainsKey(node_finish) == false)
                {
                    textBoxResult.Text = "Маршрут до указанной точки построить невозможно";
                }
                else
                { 
                    while (continue_flag) //Результирующий маршрут строится на основе значений маршрутной матрицы и значения предшествующей ей вершины.
                    {
                        textBoxResult.Text = "#" + Convert.ToString(curr) + "[" + Convert.ToString(star_list[curr].x) + "," + Convert.ToString(star_list[curr].y) + "]" + textBoxResult.Text;
                        if (road_map[curr] != -1)
                        {
                            double weight = 0;
                            foreach (Edges edge in star_list[road_map[curr]].edges)
                            {
                                if (edge.destination == curr) { weight = edge.weight; }
                            }
                            textBoxResult.Text = "-( " + Convert.ToString(weight) + " )->" + textBoxResult.Text;
                            curr = road_map[curr];
                        }
                        else { continue_flag = false; }
                    }
            } 
            sqlConnestion.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)//Кнопка активации разного ввода на форме
        {
            if (button2.Text== "Переключение на ручной ввод")
            {
                textBox1.Visible = true;
                textBox1.Enabled = true;
                textBox2.Visible = true;
                textBox2.Enabled = true;

                ComboStart.Visible = false;
                ComboStart.Enabled = false;
                ComboFinish.Visible = false;
                ComboFinish.Enabled = false;
                button2.Text = "Переключение на выпадающий список";

            }
            else
            {
                textBox1.Visible = false;
                textBox1.Enabled = false;
                textBox2.Visible = false;
                textBox2.Enabled = false;

                ComboStart.Visible = true;
                ComboStart.Enabled = true;
                ComboFinish.Visible = true;
                ComboFinish.Enabled = true;
                button2.Text = "Переключение на ручной ввод";

            }
        }

        private void button3_Click(object sender, EventArgs e)//Кнопка удаляющая из базы рёбра меньше растояния между ними на плоскости
        {
            string connectionString = Properties.Settings.Default.nodesConnectionString;
            sqlConnestion = new SqlConnection(connectionString);
            sqlConnestion.Open();
            
            SqlDataReader sqlReader1 = null;
            SqlDataReader sqlReader2 = null;

            int edge_id,node_out,node_in;
            double edge_weight;


            SqlCommand command1 = new SqlCommand("SELECT * FROM EDGES", sqlConnestion);
            try
            {
                sqlReader1 = command1.ExecuteReader();
                while (sqlReader1.Read())
                {
                    //Инициализируем все свойства отдельного ребра
                    edge_id = Convert.ToInt32(sqlReader1["ID_EDGE"]);
                    edge_weight = Convert.ToDouble(sqlReader1["WEIGHT"]);
                    node_out = Convert.ToInt32(sqlReader1["NODE_OUT"]);
                    node_in = Convert.ToInt32(sqlReader1["NODE_IN"]);
                    int x_in=0, x_out=0, y_in=0, y_out=0;
                    bool invalid_edge_flag = false;
                    for (int i=0; i<2; i++) //Получение из базы координат выходной и входной вершин
                    {

                        string str = "";
                        if (i == 0) { str = Convert.ToString(node_out); }
                        else { str = Convert.ToString(node_in); }

                        SqlCommand command2 = new SqlCommand("SELECT X,Y FROM NODES WHERE ID="+str, sqlConnestion);
                        try
                        {
                            sqlReader2 = command2.ExecuteReader();
                            while (sqlReader2.Read())
                            {
                                //Инициализируем координаты вершин которых соединяет ребро
                                if (i == 0) {
                                    x_out = Convert.ToInt32(sqlReader2["X"]);
                                    y_out = Convert.ToInt32(sqlReader2["Y"]);
                                }
                                else
                                {
                                    x_in = Convert.ToInt32(sqlReader2["X"]);
                                    y_in = Convert.ToInt32(sqlReader2["Y"]);
                                }
                            }
                            //Используя теорему Пифагора находим растояние между вершинами и сравниваем её с весом реба                            
                            if ( Math.Sqrt(Math.Pow(x_in-x_out,2)+ Math.Pow(y_in - y_out, 2))>edge_weight ) 
                            {//Вес ребра должен быть больше или равен растоянию между координатами на плоскости
                                invalid_edge_flag = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message.ToString());
                        }

                    }
                    if (invalid_edge_flag) //Если ребро не удолетворяет условию, удаляем его из базы
                    {
                        SqlCommand command2 = new SqlCommand("DELETE FROM EDGES WHERE ID_EDGE=" + Convert.ToString(edge_id), sqlConnestion);
                        try
                        {
                            sqlReader2 = command2.ExecuteReader();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message.ToString());
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            sqlConnestion.Close();
        }
    }
}
